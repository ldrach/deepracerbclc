import math

def reward_function(params):
    
    # Read input parameters
    all_wheels_on_track = params['all_wheels_on_track']
    distance_from_center = params['distance_from_center']
    progress = params['progress']
    steps = params['steps']
    speed = params['speed']
    waypoints = params['waypoints']
    closest_waypoints =  params['closest_waypoints']
    heading = params['heading']
    track_width = params['track_width']
   
    # Initialize the reward with typical value
    reward = 1e-3
    
    # Calculate the direction of the center line based on the closest waypoints
    next_point = waypoints[closest_waypoints[1]]
    prev_point = waypoints[closest_waypoints[0]]

    # Calculate the direction in radius, arctan2(dy, dx), the result is (-pi, pi) in radians
    track_direction = math.atan2(next_point[1] - prev_point[1], next_point[0] - prev_point[0])
    
    # Convert to degree
    track_direction = math.degrees(track_direction)

    # Calculate the difference between the track direction and the heading direction of the car
    direction_diff = abs(track_direction - heading)
    if direction_diff > 180:
        direction_diff = 360 - direction_diff
        
    # Prevent Zig-zag Behavior and Keep the Agent on the Track
    # Markers based on the Track Width
    marker_0 = 0.05 * track_width
    marker_1 = 0.1 * track_width
    marker_2 = 0.25 * track_width
    marker_3 = 0.5 * track_width

	# Keep the Agent on the Track and Progressing
    if (all_wheels_on_track and (steps > 0)):
        reward = ((progress / steps) * 100) + (speed ** 2)
        # Higher Reward if the Agent is Closer to Center Line and vice versa
        if (distance_from_center >= 0.0 and distance_from_center <= marker_0):
            reward += 7.5
        elif (distance_from_center <= marker_1):
            reward += 4.0
        elif (distance_from_center <= marker_2):
            reward += 2.5
        elif (distance_from_center <= marker_3):
            reward += 0.1
    else:
        reward -= 0.01

    # Penalize the reward if the difference is too large
    DIRECTION_THRESHOLD = 15.0
    if direction_diff > DIRECTION_THRESHOLD:
        reward *= 0.8

    return float(reward)