# About
This reward function was used in the BCLC Deep Racer community race. 
I was only able to get in about 3hrs of training with this model but it still managed to take 6th place
in the company race.

# Training Configuration
- **Race Type**: Time Trial
- **Environment Simulation:** Baadal Track - Counterclockwise
- **Action Space Type:** Continuous
- **Action Space:**
   - Speed: [ 1.5 : 4 ] m/s
   - Steering Angle: [ -30 : 30 ] Degrees

# Hyperparameters
- **Gradient descent batch size:** 64
- **Entropy:** 0.01
- **Discount Factor:** 0.88
- **Loss Type:** Huber
- **Learning Rate:** 0.0003
- **Number of Experience Episodes between updates:** 18
- **Number of Epochs:** 4

# Results
- **Placed:** 6th of 22 Racers
- **Training Time:** 3hrs
- **Best Lap Time:** 00:18.000
- **Gap to 1st:** +00:02.532
 
